Hi, I m Sam Nabil, founder of Naya Clinics.

When I was in Counseling school, I was deeply dissatisfied with the emphasis on what I believed to be a harsh and impersonal Cognitive Behavioral Therapy CBT approach, which was presented as the most applicable and efficient way of doing counseling.

I was convinced that those who sang the praises of CBT day and night, would steer far away from it if they needed counseling themselves or for someone they loved.

I would say to colleagues and professors With all due respect, CBT is dead

That sent me in a frenzy of research and exploration of every counseling approach under the sun, in hopes of finding the perfect one.

I was moved and impressed by the candor, humanity, and deep insights of Existential psychotherapy. At the same time, I noticed that most of the existential philosophers who wrote the body of the philosophical work that was later converted by existential therapists into existential psychotherapy were deeply

depressed and were adversely affected by horrific circumstances in their personal life, or lived in very troubled times.

Later, when their philosophical work was transformed into a therapeutic approach, the physiotherapists who pioneered and taught existential psychotherapy were equally a product of harsh personal or situational circumstances that deeply influenced their view of the world.

That to me was a big stumbling block in finding a counseling approach that I was fully convinced with. The ideas of existential psychotherapy that blossomed in an era of world war two, the holocaust, and the mass killing and destruction of the middle of the 20th century can NOT for all their wisdom be used to engage clients born in the prosperity, stability, and focus on wellness and progress that dominated the last decades of the 20th century.


And so came the birth of my personal approach to counseling Positive Existential Therapy (PET). In it, I merged the deep insights, humanity and wisdom of existential psychotherapy, with carefully curated and clinically tested elements of positive psychology, to create a counseling approach that is born of our new circumstances and conditions, and that is designed to reinvent therapy for the 21st century.

With my clients giving me fantastic feedback for the work I was doing with them using PET, I realized the importance of sharing this approach with a wider audience, and started teaching it to therapists joining my practice in Cincinnati. Quickly, we have been blessed to be Cincinnatis first choice for counseling services, and I decided to start Naya Clinics to bring my new approach to counseling to you.

I m excited to help you in whichever way we can.